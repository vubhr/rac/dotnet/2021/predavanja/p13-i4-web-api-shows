#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ShowsAPI.Models;

public class ShowsAPIContext : DbContext {
  public ShowsAPIContext(DbContextOptions<ShowsAPIContext> options)
      : base(options) {
  }

  public DbSet<ShowsAPI.Models.Show> Show { get; set; }
}
